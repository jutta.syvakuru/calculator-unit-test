#! bin/bash
cd ~
mkdir -p ~/projects/stam/L03/unit-test
code ~/projects/stam/L03/unit-test

git init
npm init -y

npm install --save express
npm install --save mocha chai

echo "node_modules" > .gitignore

# git remote add origin https://gitlab.com/jutta.syvakuru/calculator-unit-test
# git push -u origin main

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test.calc.test.js